const axios = require('axios');

class MoviesService {
    constructor() {
        this.url = "http://www.omdbapi.com/?apikey=25916a8a";
    }

    getMovieById = async (id) => {
        const movie = await axios(`${this.url}&i=${id}`);
        return movie.data;
    }

    getMovieByTitle = async (title) => {
        const movie = await axios(`${this.url}&t=${title}`);
        return movie.data;
    }

    searchMovie = async (search) => {
        const movies = await axios(`${this.url}&s=${search}`);
        return movies.data;
    }
}

module.exports = MoviesService;