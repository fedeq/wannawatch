module.exports = (app) => {
    const MoviesService = require("./services/MoviesService.js");
    const MoviesInstance = new MoviesService();

    const MoviesController = require("./controllers/MoviesController");
    const MoviesControllerInstance = new MoviesController(MoviesInstance);

    app.get('/api/movie/:id', MoviesControllerInstance.getMovieById);
    app.get('/api/movie/find/:search', MoviesControllerInstance.searchMovie);

    app.get("/", (req, res) => {
        res.sendFile(`${__dirname}/index.html`);
    })
}