class MoviesController {

    constructor(moviesInstance) {
        this.moviesInstance = moviesInstance;
    }

    getMovieById = async (req, res) => {
        const { id } = req.params;
        const movie = await this.moviesInstance.getMovieById(id);
        res.json(movie);
    }

    getMovieByTitle = (req, res) => {
        res.json({});
        
    }

    searchMovie = async (req, res) => {
        const { search } = req.params;
        const movies = await this.moviesInstance.searchMovie(search);
        res.json(movies);
        
    }
}

module.exports = MoviesController